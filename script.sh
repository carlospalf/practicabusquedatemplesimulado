#!/bin/bash

for i in {1..10};
do
	java -jar PracticaBusquedaTempleSimulado.jar > output.txt
	cat output.txt | tail -n 5 >> resul.txt
	grep -r "ENFRIAMIENTO" -i output.txt | tail -n 1 >> resEnf.txt

done

sed 's/.*ENFRIAMIENTO: \([0-9\.]*\)/\1/g' resEnf.txt > resultados.txt
cp resultados.txt resultadosEnf.txt
rm resEnf.txt
python estadisticos.py > EstadisticosEnfriamiento.txt

grep -r "FUNCION OBJETIVO" -i resul.txt > resDis.txt
sed 's/.*FUNCION OBJETIVO (km): \([0-9\.]*\)/\1/g' resDis.txt > resultados.txt
cp resultados.txt resultadosDis.txt
rm resDis.txt
python estadisticos.py > EstadisticosDistancias.txt
grep -r "ITERACION" -i resul.txt > resIt.txt
sed 's/.*ITERACION: \([0-9\.]*\)/\1/g' resIt.txt > resultados.txt
cp resultados.txt resultadosIt.txt
rm resIt.txt
python estadisticos.py > EstadisticosMejorIteracion.txt
rm output.txt
rm resul.txt
rm resultados.txt