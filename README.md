# **Práctica de búsqueda Temple Simulado**
## **Descripción**
Práctica 3 del módulo 2 de la asignatura Ingeniería del Conocimiento donde se aplicará el algoritmo de Busqueda de Temple Simulado al TSP (problema del viajante) para **n=100** ciudades.

### **Restricciones**
- La ciudad de partida y llegada tiene que ser la misma, en nuestro caso, la ciudad es 0.
- La información de las distancias se leerán de un fichero que contiene únicamente la **matriz diagonal inferior** de distancias.
  - La columna i-ésima guarda las distancias entre la ciudad i-ésima y las ciudades i+1, i+2, ...
  - La diagonal de la matriz D es 0, ya que mediría la distancia a si misma.
  - La matriz es simétrica D(i, j)= D(j, i).
- Para generar los vecinos se empleará el operador de **intercambio de dos elementos**.

### **Solución inicial**
La solución inicial será generada empleando una estrategia greedy, el rango de los índices será [1,n-1].

### **Generación de vecinos**
Se generarán todos los vecinos, para generarlos solamente se empleará el operador de **intercambio de dos elementos**. Esto nos permite un número máximo de vecinos de:

![Máximo de Vecinos1](http://latex.codecogs.com/gif.latex?%5Csum_%7Bi%3D1%7D%5E%7Bn-2%7Di%3D%5Cfrac%7B%28n-1%29*%28n-2%29%7D%7B2%7D)

### **Función de coste**
Para calcular el coste total del problema se empleará la siguiente función:

![Función de coste](http://latex.codecogs.com/gif.latex?C%28S%29%3D%20D%280%2C%20S%5B0%5D%29%20&plus;%20%5Csum_%7Bi%3D1%7D%5E%7Bn-2%7D&plus;D%28S%5Bi-1%5D%2CS%5Bi%5D%29%20&plus;%20D%28S%5Bn-2%5D%2C0%29)

### **Mecanismo de selección de soluciones candidatas**
El criterio de aceptación será el **mejor** de todos los vecinos.

### **Valor inicial del parámeto de control T<sub>0</sub>**
Para calcular el valor inicial del parámetro de control se empleará la siguiente función:

![Función de cáluclo del valor inicial del parámetro de control](https://latex.codecogs.com/gif.latex?T_%7B0%7D%3D%28%5Cfrac%7B%5Cmu%7D%7B-ln%28%5Cphi%29%7D%29%20*%20C%28S_%7B0%7D%29%20%5Ctextrm%7B%3B%20%7D%5Cmu%3D0.01%5Ctextrm%7B%2C%20%7D%5Cphi%3D0.5%5Ctextrm%7B%2C%20%7DC%28S_%7B0%7D%29%20%3D%20%5Ctextrm%7BCoste%20de%20la%20solucion%20inicial%7D9)

### **Mecanismo de Enfriamiento**
Para enfriar se emleará el esquema de Cauchy, que se puede observar en la siguiente función:

![Función de cáluclo del valor inicial del parámetro de control](https://latex.codecogs.com/gif.latex?T_%7Bk%7D%3D%5Cfrac%7BT_%7B0%7D%7D%7B%281&plus;k%29%7D%20%5Ctextrm%7B%3B%20%7Dk%3D%20%5Ctextrm%7Bnumero%20de%20veces%20que%20se%20ha%20enfriado%7D)

### **Velocidad de Enfriamiento**
Se ha decidido que se enfriará cuando:
- Se hayan aceptado 20 soluciones.
- Se hayan generado 80 soluciones candidatas desde el último enfrentamiento.

### **Criterio de parada**
Se finalizará la búsqueda cuando se realicen 10000 iteraciones de generación de solcuciones candidatas.

## **Compilación, generación de ejecutable y ejecución**
El proyecto ha sido desarrollado empleando el IDE [IntelliJ IDEA 2016.2.5](https://www.jetbrains.com/idea/), por lo que la compilación, ejecución y la generación del .jar se ha realizado empleando este IDE, aunque se ha probado a ejecutar el .jar en una terminal para comprobar el funcionamiento del .jar generado.

### **Compilación**
Dentro del IDE se seleccionará la opción **Build** y dentro de esta la opción **Make Project**.

### **Generación de ejecutable**
Para generar el ejecutable dentro del IDE se seleccionará la opción **Build** y dentro de esta la opción **Build Artifacts...** y en el menú contextual que se despliega se seleccionará **Build**.

### **Ejecución**
#### **Ejecución dentro del IDE**
Dentro del IDE se tiene la opción de indicar los parámetros de entrada seleccionando la opción **Run** y posteriormente la opción **Edit configurations...**. Una vez en la pantalla de configuración, en el textbox para **Program arguments:** se indicarán los argumentos con los que queremos realizar la ejecución separados por un espacio. En nuestro caso se introducirá lo siguiente:
- Si queremos ejecutar sin leer un fichero de aleatorios, podemos no poner ningún argumento o poner como argumento el nombre del fichero de distancias, en mi caso:
  > distancias.txt
- Si queremos leer de un fichero de aleatorios nos vemos en la obligación de indicar el fichero de distancias y el de aleatorios en este orden, en mi caso:
  > distancias.txt aleatorios.txt

Para el correcto funcionamiento es necesario que los ficheros que se van a leer estén en el contenidos en el directorio inmediato del proyecto.

#### **Ejecución en una terminal usando el .jar**
El .jar está ubicado en la carpeta **out/artifacts/PracticaBusquedaTempleSimulado_jar**, el comando para ejecutarlo es el siguiente:

> java -jar PracticaBusquedaTempleSimulado.jar

Los argumentos que se le pueden pasar y el tipo de ejecución que se realizará en función de los que se le pasen, coincide con el apartado anterior.

Para el correcto funcionamiento es necesario que los ficheros que se van a leer estén en el mismo directorio que el **.jar**.
