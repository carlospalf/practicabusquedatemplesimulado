package PracticaBusquedaTempleSimulado;

import java.util.*;

import static java.lang.Math.*;

public class Main {
    public static final int ciudades = 100; // Definimos en esta constante el número de ciudades que habrá en la búsqueda
    public static int maxVecinos = ((ciudades-1)*(ciudades-2)/2); // Variable global que lleva cuenta de el número de vecinos que hemos generado
    public static int numIteracionesTotales=10000; // Variable global que indica el número de iteraciones para mejorar el resultado que se harán
    public static int numSolucionesCandidatasGeneradas = 0; // Variable global que lleva cuenta de el número de soluciones candidatas generadas
    public static int numMaxSolucionesCandidatasGeneradas = 80; // Variable global que especifica el máximo número de soluciones candidatas generadas
    public static int numSolucionesCandidatasAceptadas = 0; // Variable global que lleva cuenta de el número de soluciones candidatas aceptadas
    public static int numMaxSolucionesCandidatasAceptadas = 20; // Variable global que especifica el máximo número de soluciones candidatas aceptadas
    public static int posAleatorios = 0; // Variable global que lleva cuenta de el número de aleaorios que se han empleado
    public static int numEnfriamientos=0;
    public static int distanciaSolucion=0;
    public static int distanciaSolucionCandidata=0;
    public static int distanciaSolucionOptima=0;
    public static int iteracionMejorSolucion=1;
    public static double mu=0.01;
    public static double phi=0.5;
    public static List<Integer> solucion    = new ArrayList<Integer>();
    public static List<Integer> solucionCandidata    = new ArrayList<Integer>();
    public static List<Integer> solucionOptima    = new ArrayList<Integer>();
    public static List<Integer> distancias  = new ArrayList<Integer>();
    public static List<Double> aleatorios  = new ArrayList<Double>();


    public static void main(String[] args) {
        int i=1,delta=0;
        double T0, T, exponencial;
        if(args.length==0){ // No se pasa ningún argumento
            Operations.distancias("distancias.txt");
            Operations.greedyInitialization();
            Operations.sobreescribirContenidoLista(solucion,solucionOptima);
            distanciaSolucion=Operations.calculoDistancia(solucion);
            distanciaSolucionOptima=distanciaSolucion;
            T0=(mu/-log(phi))*distanciaSolucion;
            T=T0;
            System.out.println("SOLUCIÓN INICIAL:");
            System.out.print("\tRECORRIDO: ");
            Operations.printSolution(solucion);
            System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucion);
            System.out.format(Locale.ENGLISH,"\tTEMPERATURA INICIAL: %.6f%n", T0);

            while(i<=numIteracionesTotales){
                while(numSolucionesCandidatasAceptadas<numMaxSolucionesCandidatasAceptadas && numSolucionesCandidatasGeneradas<numMaxSolucionesCandidatasGeneradas && i<=numIteracionesTotales) {
                    System.out.println("\nITERACION: " + i);
                    Operations.generarVecinos();
                    delta = (distanciaSolucionCandidata - distanciaSolucion);
                    exponencial = exp((-delta / T));
                    System.out.println("\tDELTA: " + delta);
                    System.out.format(Locale.ENGLISH, "\tTEMPERATURA: %.6f%n", T);
                    System.out.format(Locale.ENGLISH, "\tVALOR DE LA EXPONENCIAL: %.6f%n", exponencial);
                    if ((random() < exponencial) || (delta < 0)) {
                        Operations.sobreescribirContenidoLista(solucionCandidata, solucion);
                        distanciaSolucion = distanciaSolucionCandidata;
                        numSolucionesCandidatasAceptadas++;
                        if (distanciaSolucion < distanciaSolucionOptima) {
                            distanciaSolucionOptima = distanciaSolucion;
                            Operations.sobreescribirContenidoLista(solucion, solucionOptima);
                            iteracionMejorSolucion = i;
                        }
                        System.out.println("\tSOLUCION CANDIDATA ACEPTADA");
                    }
                    numSolucionesCandidatasGeneradas++;
                    System.out.println("\tCANDIDATAS PROBADAS: " + numSolucionesCandidatasGeneradas + ", ACEPTADAS: " + numSolucionesCandidatasAceptadas);
                    i++;
                    posAleatorios++;
                }
                if(numSolucionesCandidatasGeneradas==numMaxSolucionesCandidatasGeneradas || numSolucionesCandidatasAceptadas==numMaxSolucionesCandidatasAceptadas){
                    numEnfriamientos++;
                    T=(T0/(1+numEnfriamientos));

                    System.out.println("\n============================");
                    System.out.println("ENFRIAMIENTO: "+numEnfriamientos);
                    System.out.println("============================");
                    System.out.format(Locale.ENGLISH,"TEMPERATURA: %.6f%n", T);

                    numSolucionesCandidatasAceptadas=0;
                    numSolucionesCandidatasGeneradas=0;
                }
            }

            System.out.println("\n\nMEJOR SOLUCION: ");
            System.out.print("\tRECORRIDO: ");
            Operations.printSolution(solucionOptima);
            System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucionOptima);
            System.out.println("\tITERACION: "+ iteracionMejorSolucion);
            System.out.println("\tmu = "+mu+", phi = "+ phi);

        }
        else{
            if(args.length == 1){ // Se introdujo solo el archivo de distancias como argumento
                Operations.distancias(args[0]);
                Operations.greedyInitialization();
                Operations.sobreescribirContenidoLista(solucion,solucionOptima);
                distanciaSolucion=Operations.calculoDistancia(solucion);
                distanciaSolucionOptima=distanciaSolucion;
                T0=(mu/-log(phi))*distanciaSolucion;
                T=T0;
                System.out.println("SOLUCIÓN INICIAL:");
                System.out.print("\tRECORRIDO: ");
                Operations.printSolution(solucion);
                System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucion);
                System.out.format(Locale.ENGLISH,"\tTEMPERATURA INICIAL: %.6f%n", T0);

                while(i<=numIteracionesTotales){
                    while(numSolucionesCandidatasAceptadas<numMaxSolucionesCandidatasAceptadas && numSolucionesCandidatasGeneradas<numMaxSolucionesCandidatasGeneradas && i<=numIteracionesTotales){
                        System.out.println("\nITERACION: "+ i);
                        Operations.generarVecinos();
                        delta=(distanciaSolucionCandidata - distanciaSolucion);
                        exponencial= exp((-delta/T));
                        System.out.println("\tDELTA: "+ delta);
                        System.out.format(Locale.ENGLISH,"\tTEMPERATURA: %.6f%n", T);
                        System.out.format(Locale.ENGLISH,"\tVALOR DE LA EXPONENCIAL: %.6f%n", exponencial);
                        if ((random() < exponencial) || (delta < 0)) {
                            Operations.sobreescribirContenidoLista(solucionCandidata,solucion);
                            distanciaSolucion=distanciaSolucionCandidata;
                            numSolucionesCandidatasAceptadas++;
                            if(distanciaSolucion<distanciaSolucionOptima){
                                distanciaSolucionOptima=distanciaSolucion;
                                Operations.sobreescribirContenidoLista(solucion,solucionOptima);
                                iteracionMejorSolucion=i;
                            }
                            System.out.println("\tSOLUCION CANDIDATA ACEPTADA");
                        }
                        numSolucionesCandidatasGeneradas++;
                        System.out.println("\tCANDIDATAS PROBADAS: "+numSolucionesCandidatasGeneradas+", ACEPTADAS: "+numSolucionesCandidatasAceptadas);
                        i++;
                        posAleatorios++;
                    }
                    if(numSolucionesCandidatasGeneradas==numMaxSolucionesCandidatasGeneradas || numSolucionesCandidatasAceptadas==numMaxSolucionesCandidatasAceptadas){
                        numEnfriamientos++;
                        T=(T0/(1+numEnfriamientos));

                        System.out.println("\n============================");
                        System.out.println("ENFRIAMIENTO: "+numEnfriamientos);
                        System.out.println("============================");
                        System.out.format(Locale.ENGLISH,"TEMPERATURA: %.6f%n", T);

                        numSolucionesCandidatasAceptadas=0;
                        numSolucionesCandidatasGeneradas=0;
                    }
                }

                System.out.println("\n\nMEJOR SOLUCION: ");
                System.out.print("\tRECORRIDO: ");
                Operations.printSolution(solucionOptima);
                System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucionOptima);
                System.out.println("\tITERACION: "+ iteracionMejorSolucion);
                System.out.println("\tmu = "+mu+", phi = "+ phi);

            }
            else{ // Se introdujo el archivo de distancias y el de aleatorios como argumento (Deshabilita la inicialización greedy!!!)
                Operations.distancias(args[0]);
                Operations.aleatorio(args[1]);
                Operations.greedyInitialization();
                Operations.sobreescribirContenidoLista(solucion,solucionOptima);
                distanciaSolucion=Operations.calculoDistancia(solucion);
                distanciaSolucionOptima=distanciaSolucion;
                T0=(mu/-log(phi))*distanciaSolucion;
                T=T0;
                System.out.println("SOLUCIÓN INICIAL:");
                System.out.print("\tRECORRIDO: ");
                Operations.printSolution(solucion);
                System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucion);
                System.out.format(Locale.ENGLISH,"\tTEMPERATURA INICIAL: %.6f%n", T0);

                while(i<=numIteracionesTotales){
                    while(numSolucionesCandidatasAceptadas<numMaxSolucionesCandidatasAceptadas && numSolucionesCandidatasGeneradas<numMaxSolucionesCandidatasGeneradas && i<=numIteracionesTotales){
                        System.out.println("\nITERACION: "+ i);
                        Operations.generarVecinos();
                        delta=(distanciaSolucionCandidata - distanciaSolucion);
                        exponencial= exp((-delta/T));
                        System.out.println("\tDELTA: "+ delta);
                        System.out.format(Locale.ENGLISH,"\tTEMPERATURA: %.6f%n", T);
                        System.out.format(Locale.ENGLISH,"\tVALOR DE LA EXPONENCIAL: %.6f%n", exponencial);
                        if((aleatorios.get(posAleatorios)<exponencial) || (delta<0)){
                            Operations.sobreescribirContenidoLista(solucionCandidata,solucion);
                            distanciaSolucion=distanciaSolucionCandidata;
                            numSolucionesCandidatasAceptadas++;
                            if(distanciaSolucion<distanciaSolucionOptima){
                                distanciaSolucionOptima=distanciaSolucion;
                                Operations.sobreescribirContenidoLista(solucion,solucionOptima);
                                iteracionMejorSolucion=i;
                            }
                            System.out.println("\tSOLUCION CANDIDATA ACEPTADA");
                        }
                        numSolucionesCandidatasGeneradas++;
                        System.out.println("\tCANDIDATAS PROBADAS: "+numSolucionesCandidatasGeneradas+", ACEPTADAS: "+numSolucionesCandidatasAceptadas);
                        i++;
                        posAleatorios++;
                    }
                    if(numSolucionesCandidatasGeneradas==numMaxSolucionesCandidatasGeneradas || numSolucionesCandidatasAceptadas==numMaxSolucionesCandidatasAceptadas){
                        numEnfriamientos++;
                        T=(T0/(1+numEnfriamientos));

                        System.out.println("\n============================");
                        System.out.println("ENFRIAMIENTO: "+numEnfriamientos);
                        System.out.println("============================");
                        System.out.format(Locale.ENGLISH,"TEMPERATURA: %.6f%n", T);

                        numSolucionesCandidatasAceptadas=0;
                        numSolucionesCandidatasGeneradas=0;
                    }
                }

                System.out.println("\n\nMEJOR SOLUCION: ");
                System.out.print("\tRECORRIDO: ");
                Operations.printSolution(solucionOptima);
                System.out.println("\tFUNCION OBJETIVO (km): "+ distanciaSolucionOptima);
                System.out.println("\tITERACION: "+ iteracionMejorSolucion);
                System.out.println("\tmu = "+mu+", phi = "+ phi);

            }
        }
    }
}